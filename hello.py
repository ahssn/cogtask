# Shamelessly copied from http://flask.pocoo.org/docs/quickstart/

import socket
from flask import Flask, request
#from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello World! OO'

#if __name__ == '__main__':
#    app.run(host='0.0.0.0')

@app.route("/host/")
def return_hostname():
    return "Container's hostname: {}".format(socket.gethostname())

if __name__ == "__main__":
    app.run(host='0.0.0.0')

